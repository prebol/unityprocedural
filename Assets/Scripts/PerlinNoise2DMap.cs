using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PerlinNoise2DMap : MonoBehaviour
{
    [SerializeField]
    private int width, height;
    [SerializeField]
    private bool empty;

    [Header("Tiles to Draw")]
    public Tilemap tilemap;
    public TileBase tileGrass;
    public TileBase tileGround;
    public TileBase tileRock;
    public TileBase tileCave;
    public TileBase tileMetalOre;
    public TileBase tileGoldOre;
    public TileBase tileDiamondOre;

    [Header("Probability for every ore in ")]
    public int probabilityOfMetal;
    public int probabilityOfGold;
    public int probabilityOfDiamond;

    [Header("Smooth Setting Superface")]
    public int smoothMinSectionWidth;

    [Header("Basic Caves Random Walk")]
    public bool basicCave;
    public int requiredFloorPercent;

    [Header("Tunnel Random Walk")]
    public bool tunnelCave;
    public int minPathWidth;
    public int maxPathWidth;
    public int maxPathChange;
    public int roughness;
    public int curvyness;
    public bool goRight;

    public int probabilityTunnel;
    private static int maxTunnel=5;
    private static int tunnels;

    public int seed;

    int[,] map;

    void Start()
    {
        GenerarMapa(false);
    }

    void GenerarMapa(bool smooth) {
        //Genera el mapa básico (separa cielo y tierra)
        map = GenerateArray(width, height, empty);
        tunnels = 0;

        //Cambia la parte designada a tierra por los diferentes tipos de terrenos (Grass, Ground, Rock...)
        if (smooth) map = RandomWalkTopSmoothed(map, seed, smoothMinSectionWidth, height);
        else RandomWalkTop(map, seed, height); 

        int posX = map.GetUpperBound(0) / 2;
        int posY = 0;

        //Extras para poner cuevas o tuneles
        if (basicCave) RandomWalkCave(map, seed, requiredFloorPercent);
        if (tunnelCave) DirectionalTunnel(posX, posY, map, minPathWidth, maxPathWidth, maxPathChange, roughness, curvyness, goRight, probabilityTunnel);

        RenderMap(map, tilemap, tileGrass, tileGround, tileRock, tileCave,tileDiamondOre,tileMetalOre,tileGoldOre);

        for (int x = 0; x < map.GetUpperBound(0); x++)
        {
            for (int y = 0; y < map.GetUpperBound(1); y++)
            {
                // print($"map [{x}, {y}]: " + map[x, y]);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)) {  //Generar mapa smooth

            GenerarMapa(true);
        }
        if (Input.GetKeyDown(KeyCode.G)) {  //Generar mapa no smooth
            GenerarMapa(false);
        }
    }

    public static int[,] GenerateArray(int width, int height, bool empty)
    {
        int[,] map = new int[width, height];
        for (int x = 0; x < map.GetUpperBound(0); x++)
        {
            for (int y = 0; y < map.GetUpperBound(1); y++)
            {
                if (empty)
                {
                    map[x, y] = 0;
                }
                else
                {
                    map[x, y] = 1;
                }
            }
        }
        return map;
    }

    public static int[,] RandomWalkTop(int[,] map, float seed, int height)
    {
        //Seed our random
        System.Random rand = new System.Random(seed.GetHashCode());

        //Set our starting height
        int lastHeight = Random.Range(0, map.GetUpperBound(1));

        //Cycle through our width
        for (int x = 0; x < map.GetUpperBound(0); x++)
        {
            //Flip a coin
            int nextMove = rand.Next(2);

            //If heads, and we aren't near the bottom, minus some height
            if (nextMove == 0 && lastHeight > 2)
            {
                lastHeight--;
            }
            //If tails, and we aren't near the top, add some height
            else if (nextMove == 1 && lastHeight < map.GetUpperBound(1) - 2)
            {
                lastHeight++;
            }

            if (lastHeight < height*75/100) {
                lastHeight = height*75/100;
            }

            //Circle through from the lastheight to the bottom
            for (int y = lastHeight; y >= 0; y--)
            {
                if (y == lastHeight)
                {
                    map[x, y] = 1;  //Grass
                }
                else if (y >= lastHeight - 5)
                {
                    map[x, y] = 2;  //Dirt
                }
                else if (y < lastHeight * 20 / 100)
                {
                    int randomnew = Random.Range(0, 100);
                    if (randomnew <= 2)
                    {
                        map[x, y] = 7;  //Diamante
                    }
                    else if (randomnew <= 7)
                    {
                        map[x, y] = 6;  //Gold                    
                    }
                    else if (randomnew <= 11)
                    {
                        map[x, y] = 5;  //hierro                    
                    }
                    else
                    {
                        map[x, y] = 3;  //rock 
                    }
                }
                else if (y < lastHeight * 40 / 100)
                {
                    int randomnew = Random.Range(0, 100);
                    if (randomnew <= 5)
                    {
                        map[x, y] = 6;  //Gold
                    }
                    else if (randomnew <= 10)
                    {
                        map[x, y] = 5;  //hierro                    
                    }
                    else
                    {
                        map[x, y] = 3;  //rock                    
                    }
                }
                else if (y < lastHeight * 80 / 100) 
                {
                    int randomnew = Random.Range(0, 100);
                    if (randomnew <= 10)
                    {
                        map[x, y] = 5;  //hierro
                    }
                    else
                    {
                        map[x, y] = 3;  //rock                    
                    }
                }
                else
                {
                    int randomnew = Random.Range(0, 100);
                    if (randomnew ==45)
                    {
                        map[x, y] = 5;  //hierro
                    }
                    else
                    {
                        map[x, y] = 3;  //rock                    
                    }
                } 
            }
        }
        //Return the map
        return map;
    }

    public static int[,] RandomWalkTopSmoothed(int[,] map, float seed, int minSectionWidth, int height)
    {
        //Seed our random
        System.Random rand = new System.Random(seed.GetHashCode());

        //Determine the start position
        int lastHeight = Random.Range(0, map.GetUpperBound(1));

        //Used to determine which direction to go
        int nextMove = 0;
        //Used to keep track of the current sections width
        int sectionWidth = 0;

        //Work through the array width
        for (int x = 0; x <= map.GetUpperBound(0); x++)
        {
            //Determine the next move
            nextMove = rand.Next(2);

            //Only change the height if we have used the current height more than the minimum required section width
            if (nextMove == 0 && lastHeight > 0 && sectionWidth > minSectionWidth)
            {
                lastHeight--;
                sectionWidth = 0;
            }
            else if (nextMove == 1 && lastHeight < map.GetUpperBound(1) && sectionWidth > minSectionWidth)
            {
                lastHeight++;
                sectionWidth = 0;
            }
            //Increment the section width
            sectionWidth++;

            if (lastHeight < height*75/100) {
                lastHeight = height*75/100;
            }

            //Circle through from the lastheight to the bottom
            for (int y = lastHeight; y >= 0; y--)
            {
                if (y == lastHeight)
                {
                    map[x, y] = 1;  //Grass
                }
                else if (y >= lastHeight - 5)
                {
                    map[x, y] = 2;  //Dirt
                }
                else if (y < lastHeight * 20 / 100)
                {
                    int randomnew = Random.Range(0, 100);
                    if (randomnew <= 2)
                    {
                        map[x, y] = 7;  //Diamante
                    }
                    else if (randomnew <= 7)
                    {
                        map[x, y] = 6;  //Gold                    
                    }
                    else if (randomnew <= 11)
                    {
                        map[x, y] = 5;  //hierro                    
                    }
                    else
                    {
                        map[x, y] = 3;  //rock 
                    }
                }
                else if (y < lastHeight * 40 / 100)
                {
                    int randomnew = Random.Range(0, 100);
                    if (randomnew <= 5)
                    {
                        map[x, y] = 6;  //Gold
                    }
                    else if (randomnew <= 10)
                    {
                        map[x, y] = 5;  //hierro                    
                    }
                    else
                    {
                        map[x, y] = 3;  //rock                    
                    }
                }
                else if (y < lastHeight * 80 / 100)
                {
                    int randomnew = Random.Range(0, 100);
                    if (randomnew <= 10)
                    {
                        map[x, y] = 5;  //hierro
                    }
                    else
                    {
                        map[x, y] = 3;  //rock                    
                    }
                }
                else
                {
                    int randomnew = Random.Range(0, 100);
                    if (randomnew == 45)
                    {
                        map[x, y] = 5;  //hierro
                    }
                    else
                    {
                        map[x, y] = 3;  //rock                    
                    }
                }
            }
        }

        //Return the modified map
        return map;
    }

    public static void RenderMap(int[,] map, Tilemap tilemap, TileBase grass, TileBase ground, TileBase rock, TileBase cave,TileBase diamond, TileBase iron, TileBase gold)
    {
        //Clear the map (ensures we dont overlap)
        tilemap.ClearAllTiles();
        //Loop through the width of the map
        for (int x = 0; x < map.GetUpperBound(0) ; x++)
        {
            //Loop through the height of the map
            for (int y = 0; y < map.GetUpperBound(1); y++)
            {
                // 1 = tile, 0 = no tile
                if (map[x, y] == 1)
                {
                    tilemap.SetTile(new Vector3Int(x, y, 0), grass);
                } else if (map[x, y] == 2) {
                    tilemap.SetTile(new Vector3Int(x, y, 0), ground);
                } else if (map[x, y] == 3) {
                    tilemap.SetTile(new Vector3Int(x, y, 0), rock);
                } else if (map[x, y] == 4) {
                    tilemap.SetTile(new Vector3Int(x, y, 0), cave);
                }else if (map[x, y] == 5)
                {
                    tilemap.SetTile(new Vector3Int(x, y, 0), iron);
                }
                else if (map[x, y] == 6)
                {
                    tilemap.SetTile(new Vector3Int(x, y, 0), gold);
                }
                else if (map[x, y] == 7)
                {
                    tilemap.SetTile(new Vector3Int(x, y, 0), diamond);
                }

            }
        }
    }
    public static int[,] RandomWalkCave(int[,] map, float seed,  int requiredFloorPercent)
    {
        //Seed our random
        System.Random rand = new System.Random(seed.GetHashCode());

        //Define our start x position
        int floorX = rand.Next(1, map.GetUpperBound(0) - 1);
        //Define our start y position
        int floorY = rand.Next(1, map.GetUpperBound(1) - 1);
        //Determine our required floorAmount
        int reqFloorAmount = ((map.GetUpperBound(1) * map.GetUpperBound(0)) * requiredFloorPercent) / 100;
        //Used for our while loop, when this reaches our reqFloorAmount we will stop tunneling
        int floorCount = 0;

        //Set our start position to not be a tile (0 = no tile, 1 = tile)
        map[floorX, floorY] = 4;  
        //Increase our floor count
        floorCount++;

        while (floorCount < reqFloorAmount)
        {
            //Determine our next direction
            int randDir = rand.Next(4);

            switch (randDir)
            {
                //Up
                case 0:
                    //Ensure that the edges are still tiles
                    if ((floorY + 1) < map.GetUpperBound(1) - 1)
                    {
                        //Move the y up one
                        floorY++;

                        //Check if that piece is currently still a tile
                        if (map[floorX, floorY] == 1 || map[floorX, floorY] == 2 || map[floorX, floorY] == 3)
                        {
                            //Change it to not a tile
                            map[floorX, floorY] = 4;
                            //Increase floor count
                            floorCount++;
                        }
                    }
                    break;
                //Down
                case 1:
                    //Ensure that the edges are still tiles
                    if ((floorY - 1) > 1)
                    {
                        //Move the y down one
                        floorY--;
                        //Check if that piece is currently still a tile
                        if (map[floorX, floorY] == 1 || map[floorX, floorY] == 2 || map[floorX, floorY] == 3)
                        {
                            //Change it to not a tile
                            map[floorX, floorY] = 4;
                            //Increase the floor count
                            floorCount++;
                        }
                    }
                    break;
                //Right
                case 2:
                    //Ensure that the edges are still tiles
                    if ((floorX + 1) < map.GetUpperBound(0) - 1)
                    {
                        //Move the x to the right
                        floorX++;
                        //Check if that piece is currently still a tile
                        if (map[floorX, floorY] == 1 || map[floorX, floorY] == 2 || map[floorX, floorY] == 3)
                        {
                            //Change it to not a tile
                            map[floorX, floorY] = 4;
                            //Increase the floor count
                            floorCount++;
                        }
                    }
                    break;
                //Left
                case 3:
                    //Ensure that the edges are still tiles
                    if ((floorX - 1) > 1) 
                    {
                        //Move the x to the left
                        floorX--;
                        //Check if that piece is currently still a tile
                        if (map[floorX, floorY] == 1 || map[floorX, floorY] == 2 || map[floorX, floorY] == 3)
                        {
                            //Change it to not a tile
                            map[floorX, floorY] = 4;
                            //Increase the floor count
                            floorCount++;
                        }
                    }
                    break;
            }
        }

        //Return the updated map
        return map;
    }

    public static int[,] DirectionalTunnel(int posX, int posY, int[,] map, int minPathWidth, int maxPathWidth, int maxPathChange, int roughness, int curvyness, bool goRight, int probabilityTunnel)
    {
        //This value goes from its minus counterpart to its positive value, in this case with a width value of 1, the width of the tunnel is 3
        int tunnelWidth = 1;
        //Set the start X position to the center of the tunnel
        int x = map.GetUpperBound(0) / 2;

        //Set up our random with the seed
        System.Random rand = new System.Random(Time.time.GetHashCode());

        if (!goRight) { //VA HACIA LA IZQUIERDA
            print("TUNNEL GOING TO LEFT");
            //Create the first part of the tunnel
            for (int i = -tunnelWidth; i <= tunnelWidth; i++)
            {
                if (map[x + i, 0] >= 1) map[x + i, 0] = 4;
            }

            //Cycle through the array
            for (int y = 1; y < map.GetUpperBound(1); y++)
            {
                float random = Random.Range(0, y+probabilityTunnel);
                if (random == 1) tryToAddTunnel(x, y, map, minPathWidth, maxPathWidth, maxPathChange, roughness, curvyness, probabilityTunnel);
                //Check if we can change the roughness
                if (rand.Next(0, 100) > roughness)
                {
                    //Get the amount we will change for the width
                    int widthChange = Random.Range(-maxPathWidth, maxPathWidth);
                    //Add it to our tunnel width value
                    tunnelWidth += widthChange;
                    //Check to see we arent making the path too small
                    if (tunnelWidth < minPathWidth)
                    {
                        tunnelWidth = minPathWidth;
                    }
                    //Check that the path width isnt over our maximum
                    if (tunnelWidth > maxPathWidth)
                    {
                        tunnelWidth = maxPathWidth;
                    }
                }

                //Check if we can change the curve
                if (rand.Next(0, 100) > curvyness)
                {
                    //Get the amount we will change for the x position
                    int xChange = Random.Range(-maxPathChange, maxPathChange);
                    //Add it to our x value
                    x += xChange;
                    //Check we arent too close to the left side of the map
                    if (x < maxPathWidth)
                    {
                        x = maxPathWidth;
                    }
                    //Check we arent too close to the right side of the map
                    if (x > (map.GetUpperBound(0) - maxPathWidth))
                    {
                        x = map.GetUpperBound(0) - maxPathWidth;
                    }
                }

                //Work through the width of the tunnel
                for (int i = -tunnelWidth; i <= tunnelWidth; i++)
                {
                    if (map[x + i, y] >= 1) map[x + i, y] = 4;  //Cave
                }
                

            }
        } else {     //VA HACIA LA DERECHA
            print("TUNNEL GOING TO RIGHT");
            //Create the first part of the tunnel
            for (int i = tunnelWidth; i > -tunnelWidth; i--)
            {
                if (map[x - i, 0] >= 1) map[x - i, 0] = 4;
            }

            //Cycle through the array
            for (int y = 1; y < map.GetUpperBound(1); y++)  //Tunnel going up
            {

                float random = Random.Range(0, y + probabilityTunnel);
                if (random == 1) tryToAddTunnel(x, y, map, minPathWidth, maxPathWidth, maxPathChange, roughness, curvyness, probabilityTunnel);
                //Check if we can change the roughness
                if (rand.Next(0, 100) > roughness)
                {
                    //Get the amount we will change for the width
                    int widthChange = Random.Range(-maxPathWidth, maxPathWidth);
                    //Add it to our tunnel width value
                    tunnelWidth -= widthChange;
                    //Check to see we arent making the path too small
                    if (tunnelWidth < minPathWidth)
                    {
                        tunnelWidth = minPathWidth;
                    }
                    //Check that the path width isnt over our maximum
                    if (tunnelWidth > maxPathWidth)
                    {
                        tunnelWidth = maxPathWidth;
                    }
                }

                //Check if we can change the curve
                if (rand.Next(0, 100) > curvyness)
                {
                    //Get the amount we will change for the x position
                    int xChange = Random.Range(-maxPathChange, maxPathChange);
                    //Add it to our x value
                    x -= xChange;
                    //Check we arent too close to the left side of the map
                    if (x < maxPathWidth)
                    {
                        x = maxPathWidth;
                    }
                    //Check we arent too close to the right side of the map
                    if (x > (map.GetUpperBound(0) - maxPathWidth))
                    {
                        x = map.GetUpperBound(0) - maxPathWidth;
                    }
                }

                //Work through the width of the tunnel
                for (int i = tunnelWidth; i > -tunnelWidth; i--)
                {
                    if (map[x - i, y] >= 1) map[x - i, y] = 4;  //Cave
                }

            }
        }
        return map;
    }

    public static void tryToAddTunnel(int posX, int posY, int[,] map, int minPathWidth, int maxPathWidth, int maxPathChange, int roughness, int curvyness, int probabilityTunnel) {
        float rand = Random.Range(0, 2);
        bool flag = default;
        if (rand == 0) flag = false;
        else flag = true;
        tunnels++;
        if (tunnels < maxTunnel) DirectionalTunnel(posX, posY, map, minPathWidth, maxPathWidth, maxPathChange, roughness, curvyness, flag, probabilityTunnel);
    }

}
